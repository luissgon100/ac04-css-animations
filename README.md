# AC04 CSS Animations
Simulació de una web per a concerts, aplicació de html y css.
Requisits: 
Aplica les animacions i transformacions necessàries per aconseguir el següent efecte:
En carregar-se la pàgina, només es mostraran els requadres amb les imatges, ocultant el títol, text, enllaç a "Read
more" i el fons blau.
En passar amb el ratolí sobre la imatge, ha de passar el següent:
 La imatge de fons augmentarà la mida, fent la impressió que s'acosta.
 Es mostrarà el fons blau, de manera progressiva amb una opacitat per deixar veure la imatge que hi ha a
sota.
 El títol apareixerà de manera progressiva des d'opacitat 0 a 1.
 El paràgraf apareixerà de manera progressiva des d'opacitat 0 a 1.
 Es mostrarà l'enllaç "Read more", de manera progressiva des d'opacitat 0 a 1.

El web ha de ser responsive, mostra 3, 2 o 1 columna depenent de l’amplada del navegador.
